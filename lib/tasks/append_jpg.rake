namespace :db do
  task artwork_add_ext_jpg: :environment do
    Artwork.all.each do |art|
      unless art.image.end_with? '.jpg'
        art.image << '.jpg'
        art.save
      end
    end
  end
end