# Quick and dirty, this makes a lot of assumptions about the structure of the data
# and doesn't worry about concurrent commits made while running this taks.
# This is a migration from production in an older database to the current on.
# All future records will be created in the new system.
require 'csv'
namespace :db do
  task old_art_db_import: :environment do
    raise 'Data already migrated.' if Artwork.count > 0
    CSV.open('lib/tasks/art_dump.csv').drop(1).each do |row|
      Artwork.create(
                 name: row[1],
                 description: row[2],
                 height: row[3].to_f,
                 width: row[4].to_f,
                 price: row[5].to_f,
                 sold: row[6].to_i,
                 image: File.open("public/images/artworks/artwork/#{row[7]}.jpg")
      )
    end
  end
end