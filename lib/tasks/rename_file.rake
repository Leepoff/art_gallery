task :remove_thb_filename do
  Dir.glob('public/images/artworks/thumbnails/*.jpg') do |file|
    File.rename(file, file.gsub('_thb.jpg', '.jpg'))
  end
end