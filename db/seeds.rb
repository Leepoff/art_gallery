Artwork.create!([
  {name: "Smoke on the Mountain", description: "Acrylic on canvas", height: "36.0", width: "24.0", price: "0.0", sold: true, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Seascape", description: "Acrylic on Canvas", height: "14.0", width: "18.0", price: "0.0", sold: true, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Watermelon Fan", description: "Pen & Ink and watercolour", height: "11.0", width: "9.0", price: "0.0", sold: true, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Antique Fan", description: "Water colour", height: "11.0", width: "9.0", price: "150.0", sold: false, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Sunflower Vase", description: "sunflower in the earth vase", height: "30.0", width: "30.0", price: "0.0", sold: true, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Irises & Dragonfly", description: "Pastels Irises and dragonfly at Audubon park", height: "11.0", width: "9.0", price: "150.0", sold: false, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "First Day of Spring", description: "Oil Painting", height: "36.0", width: "24.0", price: "3000.0", sold: false, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Oak Tree", description: "Acrylic on Canvas", height: "20.0", width: "28.0", price: "550.0", sold: false, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Mermaid", description: "", height: "12.0", width: "9.0", price: "0.0", sold: true, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Ode to Klimt`s Kiss", description: "Colored pencil, marker and gold foil on paper", height: "9.0", width: "12.0", price: "0.0", sold: true, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Valentines Day Inspired", description: "", height: "20.0", width: "16.0", price: "0.0", sold: true, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Reflections", description: "", height: "20.0", width: "16.0", price: "500.0", sold: false, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Celestial Garden", description: "", height: "36.0", width: "24.0", price: "500.0", sold: true, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Sunrise at Angel's Gate", description: "", height: "36.0", width: "24.0", price: "500.0", sold: true, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Sunflower", description: "", height: "24.0", width: "18.0", price: "700.0", sold: false, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Celestial Vibrations", description: "", height: "24.0", width: "18.0", price: "600.0", sold: false, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Cosmic Daisies", description: "", height: "24.0", width: "18.0", price: "600.0", sold: false, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Reflections in Green and Blue", description: "Acrylic Abstract", height: "36.0", width: "24.0", price: "500.0", sold: false, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "mountainlines", description: "water color", height: "14.0", width: "12.0", price: "350.0", sold: false, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Green Earth", description: "", height: "9.0", width: "11.0", price: "75.0", sold: false, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "fall is here", description: " markers", height: "9.0", width: "7.0", price: "250.0", sold: false, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Golden Orange", description: "markers", height: "9.0", width: "7.0", price: "250.0", sold: false, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Klimt Tree", description: "markers", height: "12.0", width: "9.0", price: "250.0", sold: false, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Evolving Series", description: "Pen and Ink", height: "15.0", width: "11.0", price: "275.0", sold: false, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Evolving Series", description: "Pen and Ink", height: "15.0", width: "11.0", price: "275.0", sold: false, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "First of the Evolution", description: "Pen and Ink", height: "15.0", width: "11.0", price: "275.0", sold: false, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Evolving Series", description: "Pen and Ink", height: "9.0", width: "7.0", price: "175.0", sold: false, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Evolving Series", description: "Pen and Ink", height: "14.0", width: "11.0", price: "250.0", sold: false, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Exploring the Heart ", description: "pen and ink", height: "11.0", width: "9.0", price: "350.0", sold: false, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "wisdom, power and love", description: "pen and ink", height: "11.0", width: "9.0", price: "250.0", sold: false, image: File.open("spec/factories/test_img.jpg"), category_id: nil},
  {name: "Gardenica", description: "", height: "12.0", width: "12.0", price: "0.0", sold: false, image: File.open("spec/factories/test_img.jpg"), category_id: nil}
])
Category.create!([
  {name: "oil", description: nil},
  {name: "pastel", description: nil},
  {name: "watercolor", description: nil}
])

# Password: password
User.create!([
  {name: "test", email: "one@two.three", password_digest: "$2a$10$we3keE2KplLSx1IQfLODNuxVZuE8MQTJv1T/l9XmDF/8/RcmdHzOm", remember_digest: nil, admin: true}
])