class CreateArtworks < ActiveRecord::Migration
  def change
    create_table :artworks do |t|
      t.string  :name
      t.text    :description
      t.decimal :height,  precision:5, scale: 2
      t.decimal :width, precision:5,  scale: 2
      t.decimal :price,  precision:15, scale: 2
      t.boolean :sold,    default: false
      t.string  :image
      t.belongs_to :category, index: true

      t.timestamps
    end
  end
end
