module ArtworksHelper

  def thumb_link artwork
    link_to image_tag(artwork.image.thumb.url), artwork
  end

  def art_img_link artwork
    link_to image_tag(artwork.image_url), artwork
  end

end
