class ArtworksController < ApplicationController
  before_action :set_artwork, only: [:show, :edit, :update, :destroy]
  before_action :set_pagination, only: [:index, :new, :edit, :create, :update]
  before_action :authorize_admin, except: [:index, :show]
  # GET /artworks
  def index
  end

  # GET /artworks/1
  # GET /artworks/1.json
  def show
  end

  # GET /artworks/new
  def new
    @artwork = Artwork.new
  end

  # GET /artworks/1/edit
  def edit
  end

  # POST /artworks
  def create
    @artwork = Artwork.new(artwork_params)
    if @artwork.save
      redirect_to edit_artwork_path(@artwork.id), notice: 'New art piece created.'
    else
      render :new
    end
  end

  # PATCH/PUT /artworks/1
  def update
    if @artwork.update(artwork_params)
      redirect_to edit_artwork_path, notice: 'Artwork was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /artworks/1
  def destroy
    @artwork.destroy
    redirect_to new_artwork_url, notice: 'Artwork was successfully destroyed.'
  end

  private
    def authorize_admin
      redirect_to root_path unless is_admin?
    end

    def set_artwork
      @artwork = Artwork.find(params[:id])
    end

  def set_pagination
    @artworks = Artwork.paginate(:page => params[:page], :per_page => 6)
  end

    def artwork_params
      params.require(:artwork).permit(:category_id, :name, :description, :width, :height, :sold, :price, :image)
    end
end
