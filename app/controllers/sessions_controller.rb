class SessionsController < ApplicationController
  def new
  end

  def create
    @user = User.find_by(email: session_params[:email].downcase)
    if @user && @user.authenticate(session_params[:password])
      log_in @user
      session_params[:remember_me] == '1' ? remember(@user) : forget(@user)
      redirect_to new_artwork_path, flash: {success: "Successfully logged in."}
    else
      flash.now[:danger] = "Incorrect email/password."
      render :new
    end
  end

  def destroy
    log_out
    redirect_to root_path, flash: {success: "Successfully logged out."}
  end

  private

  def session_params
    params.require(:session).permit(:email, :password, :remember_me)
  end
end
