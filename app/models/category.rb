class Category < ActiveRecord::Base
  #Create a couple categories, with 0 being a "no category" category

  has_many :artworks
  validates :name, presence: true, uniqueness: true

end
