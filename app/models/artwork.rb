class Artwork < ActiveRecord::Base
  belongs_to :category

  mount_uploader :image, ArtworkImagesUploader

  validate :width_height_presence_mutuality
  validate :image_size

  validates :width, :height, numericality: true, allow_blank: true
  validates :name, presence: true
  validates :image, presence: {message: "file to upload is needed."}

  private

    def width_height_presence_mutuality
      if width && height
        return
      elsif width
        errors.add(:height, "cannot be empty with a width given.")
      elsif height
        errors.add(:width, "cannot be empty with a height given.")
      end
    end

    def image_size
      if image.size > 5.megabytes
        errors.add(:image, "should be less than 5MB")
      end
    end

end
