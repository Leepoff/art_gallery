# Netta's Online Art Gallery

This is a simple yet solid online app for displaying and managing artwork.

Administrative login can be reached by going to your http://<your localhost>/login
(SSL will be configured before going live)

Tests are using RSpec with Capybara

```ruby
$ bin/rspec spec
```

Seed data is available with a test image for proper site laytout
```ruby
$ rake db:seed
```

Administrative password is `password`


Docker files are available at: <docker file url>

