require 'rails_helper'

RSpec.describe User, type: :model do

  it { is_expected.to validate_presence_of(:email)}
  it { is_expected.to validate_length_of(:email).is_at_most(255)}
  #confused right now how shoulda saves a user with an invalid email to check the uniqueness
  it { is_expected.to validate_uniqueness_of(:email).ignoring_case_sensitivity}

  #email format validation
  it { is_expected.to allow_value('user@example.com', 'USER@foo.COM', 'A_US-ER@foo.bar.org',
                                  'first.last@foo.jp', 'alice+bob@baz.cn', 'hello@me.nz.gov').for(:email)}
  it { is_expected.to_not allow_value('user@example,com', 'user_at_foo.org', 'user.name@example.',
                                      'foo@bar_baz.com', 'foo@bar+baz.com', 'double_dott@test..com').for(:email)}

  it { is_expected.to validate_presence_of(:name)}
  it { is_expected.to validate_length_of(:name).is_at_most(255)}

  it { is_expected.to validate_presence_of(:password)}
  it { is_expected.to validate_length_of(:password).is_at_least(8)}

  it "should function if the user remember_digest is empty but the remember_token isn't" do
    user = create(:user, remember_digest:nil, remember_token: User.new_token)
    expect(user.authenticated?(user.remember_token)).to be false
  end

end