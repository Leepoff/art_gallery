require 'rails_helper'

describe Artwork do

  it { is_expected.to validate_presence_of(:name)}
  it { is_expected.to validate_presence_of(:image).with_message('file to upload is needed.')}

  it { is_expected.to validate_numericality_of(:width)}
  it { is_expected.to validate_numericality_of(:height)}

  it "checks that height can't be empty with a width given" do
    artwork = FactoryGirl.build(:artwork, height:nil)
    artwork.valid?
    expect(artwork.errors[:height]).to include('cannot be empty with a width given.')
  end

  it "checks that width can't be empty with a height given" do
    artwork = FactoryGirl.build(:artwork, width:nil)
    artwork.valid?
    expect(artwork.errors[:width]).to include('cannot be empty with a height given.')
  end
end
