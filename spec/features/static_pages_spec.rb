require 'rails_helper'

feature 'static pages' do

  scenario 'visiting the Home page' do
    create(:artwork)
    visit root_path
    expect(page).to have_image('test_img.jpg')
  end

  scenario 'visiting the About page' do
    visit about_path
    expect(page).to have_image('netta_about')
  end

  scenario 'visiting the Contact page' do
    visit contact_path
    expect(page).to have_image('site_mail')
  end

end