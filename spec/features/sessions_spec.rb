require 'rails_helper'

feature 'Session management' do

  background do
    @user = create(:user)
  end

  scenario "login with valid credentials" do
    log_in_as(@user)
    expect(find('.alert')).to have_content "Successfully logged in."
    expect(current_path).to eq root_path
  end

  scenario "logout user" do
    visit root_path
    expect(page).not_to have_link logout_path
    log_in_as(@user)
    expect(page).to have_link "Log out"
    click_link "Log out"
    expect(current_path).to eq root_path
    expect(find('.alert')).to have_content "Successfully logged out"
    expect(page).not_to have_link "Log out"
  end

  scenario "clicking log out after logging out in another window doesn't cause an error", js: true do
    log_in_as(@user)

    within_window open_new_window do
       visit root_path
       click_link "Log out"
    end

    click_link "Log out"
    expect(find('.alert')).to have_content "Successfully logged out"
  end

  scenario "unchecking remember me forgets user on browser restart" do
    log_in_as(@user, remember_me: false)
    expire_cookies
    visit root_path
    expect(page).not_to have_link "Log out"
  end

  scenario "checking remember me remembers user on browser restart" do
    log_in_as(@user, remember_me: true)
    expire_cookies
    visit root_path
    expect(page).to have_link "Log out"
  end

  scenario "login with invalid credentials" do
    @user.email = ""
    log_in_as(@user)
    expect(current_path).to eq login_path
    click_button "Login"
    expect(current_path).to eq login_path
    expect(page).to have_css '.alert-danger'
    visit root_path
    expect(page).not_to have_css '.alert-danger'
  end
end