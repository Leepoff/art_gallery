require 'rails_helper'

feature 'Artwork management' do
  # let(:blog_post) { create(:blog)}
  #
  # describe 'home page' do
  #   before do
  #     visit root_path
  #   end
  #
  #   it 'should display the home page' do
  #     expect(page).to have_content blog_post.content
  #   end
  # end

  #Need to create logged in user for tests

  scenario "anonymous user attempts to access artwork crud" do
    visit new_artwork_path
    expect(current_path).to eq root_path
    visit edit_artwork_path(create(:artwork))
    expect(current_path).to eq root_path
  end

  scenario "logged in non-admin user attempts to access artwork crud" do
    @user = create(:user)
    log_in_as(@user)
    visit new_artwork_path
    expect(current_path).to eq root_path
    visit edit_artwork_path(create(:artwork))
    expect(current_path).to eq root_path
  end

  feature 'logged in as an admin' do
    background do
      @admin = create(:admin)
      log_in_as(@admin)
    end
    scenario 'adding new artwork' do
      artwork = FactoryGirl.build(:artwork)
      visit new_artwork_path
      expect{
        fill_in 'Name', with: artwork.name
        fill_in 'Description', with: artwork.description
        fill_in 'Width', with: artwork.width
        fill_in 'Height', with: artwork.height
        select 'oil', from: 'Category'
        check 'Sold'
        fill_in 'Price', with: artwork.price
        attach_file 'Image', "#{Rails.root}/spec/factories/test_img.jpg"
        click_button 'Create Artwork'
      }.to change(Artwork, :count).by(1)
      expect(current_path).to eq edit_artwork_path(Artwork.last.id)
      expect(page).to have_content 'New art piece created.'
    end

    scenario "editing artwork" do
      visit new_artwork_path
      artwork = FactoryGirl.build(:artwork)
      fill_in 'Name', with: artwork.name
      attach_file 'Image', "#{Rails.root}/spec/factories/test_img.jpg"
      click_button 'Create Artwork'

      visit artwork_path(Artwork.last.id)
      expect(page).to have_content Artwork.name

      visit edit_artwork_path(Artwork.last.id)
      fill_in 'Name', with: 'new name'
      click_button 'Update Artwork'
      visit artwork_path(Artwork.last.id)
      expect(page).to have_content 'new name'
    end


    scenario "errors when adding artwork" do
      visit new_artwork_path
      expect{
        click_button 'Create Artwork'
      }.not_to change(Artwork, :count)
      expect(current_path).to eq artworks_path
      expect(page).to have_content "Name can't be blank"
      expect(page).to have_content "Image file to upload is needed."
    end


    scenario 'deleting artwork' do
      visit new_artwork_path
      artwork = FactoryGirl.build(:artwork)

      fill_in 'Name', with: artwork.name
      attach_file 'Image', "#{Rails.root}/spec/factories/test_img.jpg"
      click_button("Create Artwork")
      find('#artwork_manage_list').find('img').click
      expect{click_button('Delete')}.to change(Artwork, :count).by(-1)
    end
  end

  pending 'test max file upload size of 5MB (need to configure javascript for alert box)'

  pending 'issue with admin logging in when more than one user in DB'

end