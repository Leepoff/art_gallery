RSpec::Matchers.define :have_image do |expected|
  match do |actual|
    actual.has_xpath?("//img[contains(@src,\"#{expected}\")]")
  end
end