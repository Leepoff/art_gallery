module LoginMacros
  def log_in(user)
    session[:user_id] = user.id
  end

  def log_in_as(user, remember_me: true)
    visit login_path
    fill_in 'Email', with: user.email
    fill_in 'Password', with: 'password'
    if remember_me
      check 'Remember me'
    else
      uncheck 'Remember me'
    end
    click_button 'Login'
  end
end