FactoryGirl.define do
  factory :category do
    name "Oil"
    description "Oil paintings"
  end
end
