FactoryGirl.define do
  factory :user do
    name "MyString"
    email "example@example.com"
    password "password"
    factory :user_with_invalid_email do
      email "example_at_example.com"
    end

    factory :admin do
      email 'admin@example.com'
      admin true
    end

  end


  # factory :valid_user, class: User do
  #   name "MyString"
  #   email "example@example.com"
  #   password "password"
  # end
  #
  #
  # factory :invalid_email_user, class: User do
  #   name "MyString"
  #   email "example_at_example.com"
  # end
end
