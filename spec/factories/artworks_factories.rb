require 'faker'
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :artwork do
    sequence(:name) { |n| "artpiece#{n}" }
    image { Rack::Test::UploadedFile.new(File.open("#{Rails.root}/spec/factories/test_img.jpg"), 'image/jpg') }
    #image File.open("#{Rails.root}/spec/factories/test_img.jpg")
    description { Faker::Lorem.sentence }
    width { rand(1.00...100.00).round(2) }
    height { rand(1.00...100.00).round(2) }
    sold [true, false].sample
    price { rand(1.00..10000.00).round(2) }
  end
end
