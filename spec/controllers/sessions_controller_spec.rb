require 'rails_helper'

RSpec.describe SessionsController, type: :controller do

  describe "GET #new" do
    it "renders the new template" do
      get :new
      expect(response).to render_template :new
    end
  end

  describe "POST #create" do
    before :each do
      @user = create(:admin)
    end
    context "with valid credentials" do
      it "redirects to the new artwork page" do
        post :create, session: attributes_for(:admin)
        expect(response).to redirect_to new_artwork_path
      end
      it "assigns the correct User to @user" do
        post :create, session: attributes_for(:admin)
        expect(assigns(:user)).to eq @user
      end
      it 'properly assigns the remember token' do
        expect(@user.remember_digest).to be_nil
        post :create, session: {email:@user.email, password:@user.password, remember_me: '1'}
        @user.reload
        expect(@user.remember_digest).not_to be_nil
      end
    end
    context "with invalid credentials" do
      it "renders the :new template" do
        post :create, session: attributes_for(:user_with_invalid_email)
        expect(response).to render_template :new
      end
      it 'does not assign a User to @user' do
        post :create, session: attributes_for(:user_with_invalid_email)
        expect(assigns(:user)).to be_nil
      end
    end
  end

  describe "DELETE #destroy" do
    it 'removes the session id' do
      user = create(:admin)
      log_in(user)
      expect(session[:user_id]).to eq user.id
      delete :destroy
      expect(session[:user_id]).to be_nil
    end

    it 'redirects to root path with a notice' do
      delete :destroy
      expect(response).to redirect_to root_path
      expect(flash[:success]).not_to be_nil
    end
  end

end
