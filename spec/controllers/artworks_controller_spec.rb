require 'rails_helper'

RSpec.describe ArtworksController, type: :controller do

  shared_examples 'publicly accessible routes' do
    describe 'GET #index' do
      it 'populates an array of 6 art pieces' do
        create_list(:artwork, 10)
        get :index
        artworks = Artwork.limit(6)
        expect(assigns(:artworks)).to match_array(artworks)
      end

      it 'renders the :index template' do
        get :index
        expect(response).to render_template :index
      end
    end

    describe 'GET #show' do
      it 'assigns the requested artwork to @artwork' do
        artwork = create(:artwork)
        get :show, id: artwork
        expect(assigns(:artwork)).to eq artwork
      end
      it 'renders the :show template' do
        artwork = create(:artwork)
        get :show, id: artwork
        expect(response).to render_template :show
      end
    end

  end

  shared_examples 'non-admin access to routes' do
    before :each do
      @artwork = create(:artwork)
    end

    describe 'GET #new' do
      it 'redirects to root' do
        get :new
        expect(response).to redirect_to root_path
      end
    end

    describe 'POST #create' do
      it 'redirects to root' do
        post :create
        expect(response).to redirect_to root_path
      end
    end

    describe 'PATCH #update' do
      it 'redirects to root' do
        patch :update, id: @artwork.id
        expect(response).to redirect_to root_path
      end
    end

    describe 'DELETE #destroy' do
      it 'redirects to root' do
        delete :destroy, id: @artwork.id
        expect(response).to redirect_to root_path
      end
    end
  end

  #*******************************************
  #******* Anonymous user access tests *******
  #*******************************************
  describe 'Anonymous access' do
    it_behaves_like 'publicly accessible routes'
    it_behaves_like 'non-admin access to routes'
  end

  #*******************************************
  #******* User access tests *****************
  #*******************************************
  #Currently there is no user feature, only reason for a log in is for admin management
  describe 'user access routes' do
    before :each do
      user = create(:user)
      session[:user_id] = user.id
    end
    it_behaves_like 'publicly accessible routes'
    it_behaves_like 'non-admin access to routes'
  end

  #*******************************************
  #******* Administrator access tests ********
  #*******************************************
  describe 'Administrator access' do
    before :each do
      admin = create(:admin)
      session[:user_id] = admin.id
    end

    it_behaves_like 'publicly accessible routes'

    describe 'GET #new' do
      it 'assigns a new Artwork to @artwork' do
        get :new
        expect(assigns(:artwork)).to be_a_new(Artwork)
      end

      it 'renders the :new template' do
        get :new
        expect(response).to render_template :new
      end
    end

    describe 'GET #edit' do
      it 'assigns the requested artwork' do
        artwork = create(:artwork)
        get :edit, id: artwork
        expect(assigns(:artwork)).to eq artwork
      end

      it 'renders the :edit template' do
        artwork = create(:artwork)
        get :edit, id: artwork
        expect(response).to render_template :edit
      end
    end

    describe 'POST #create' do
      context 'with valid attributes' do
        it 'creates a new Artwork' do
          expect{post :create, artwork: attributes_for(:artwork)}.to change(Artwork, :count).by(1)
        end
        it 'renders the edit page' do
          post :create, artwork: attributes_for(:artwork)
          expect(response).to redirect_to edit_artwork_path(assigns(:artwork))
        end
      end
      context 'with invalid attributes' do
        it 'does not create a new Artwork' do
          expect{post :create, artwork: attributes_for(:artwork, name:'')}.to change(Artwork, :count).by(0)
        end
        it 'renders the :new template' do
          post :create, artwork: attributes_for(:artwork, name:'')
          expect(response).to render_template :new
        end
      end
    end

    describe 'PATCH #update' do
      before :each do
        @artwork = create(:artwork)
      end
      context 'with valid attributes' do

        it 'updates the Artwork' do
          patch :update, id: @artwork.id, artwork: attributes_for(:artwork, name: 'new name')
          @artwork.reload
          expect(@artwork.name).to eq 'new name'
        end
        it 'redirects to the new artwork edit page' do
          patch :update, id: @artwork.id, artwork: attributes_for(:artwork, name: 'new name')
          expect(response).to redirect_to edit_artwork_path(@artwork)
        end
      end
      context 'with invalid attributes' do
        it 'does not update the Artwork' do
          patch :update, id: @artwork.id, artwork: attributes_for(:artwork, name: '')
          @artwork.reload
          expect(@artwork.name).not_to eq ''
        end
        it 'renders the :edit template' do
          patch :update, id: @artwork.id, artwork: attributes_for(:artwork, name: '')
          expect(response).to render_template :edit
        end
      end
    end

    describe 'DELETE #destroy' do
      before :each do
        @artwork = create(:artwork)
      end
      it 'deletes the artwork' do
        expect{delete :destroy, id: @artwork.id}.to change(Artwork, :count).by(-1)
      end
      it 'renders redirects to the new artwork page' do
      delete :destroy, id: @artwork.id
      expect(response).to redirect_to new_artwork_path
      end
    end
  end

end
