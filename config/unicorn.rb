root = '/app'

worker_processes 3
user "root"
preload_app true
timeout 30

listen "#{root}/tmp/sockets/unicorn.sock", backlog: 64
pid "#{root}/tmp/pids/unicorn.pid"
working_directory "#{root}"
stderr_path "#{root}/log/unicorn.log"
stdout_path "#{root}/log/unicorn.log"